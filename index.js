const time = document.getElementById("time");
const date = document.getElementById("date");
addEventListener("load", (_ev) => {
    update_datetime();
    setInterval(update_datetime, 1000);
});

const update_datetime = () => {
    console.log("update");
    let now = new Date();
    time.innerText = `${now.getHours().toString().padStart(2, "0")}:${now
        .getMinutes()
        .toString()
        .padStart(2, "0")}:${now.getSeconds().toString().padStart(2, "0")}`;
    date.innerText = `${now.getFullYear()}-${(now.getMonth() + 1)
        .toString()
        .padStart(2, "0")}-${now.getDate().toString().padStart(2, "0")}`;
};

if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) {
    document.querySelector("html").setAttribute("data-theme", "dark");
} else {
    document.querySelector("html").setAttribute("data-theme", "light");
}
